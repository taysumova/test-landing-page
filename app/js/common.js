$(function() {
	// Custom JS

  // For modal form - open
  $(".buttons, .banner").on('click', '#callback', function(){
    $(".callback-modal").addClass("callback-modal--active");
  });
  // For modal form - close
  $(".callback-modal").on('click', '.form__close', function () {
    $(".callback-modal").removeClass("callback-modal--active");
  })

  // For question - answers
  $(".questions__choice, object").on('click', function () {
    alert('Hi');
    $('path').css({ fill: "#fff" });
    $('.questions__answer').removeClass('active');
    $(this).removeClass('questions__choice--active');
    $('.questions__' + $(this)[0].id).addClass('active');
    $('#icon__' + $(this)[0].id).addClass('questions__icon--active').load(location.href + '#icon__' + $(this)[0].id);
    // $('#icon__' + $(this)[0].id)
    $(this).addClass('questions__choice--active');
    let xhr = new XMLHttpRequest();
    xhr.open("GET","icon_question_1.svg",false);
// Following line is just to be on the safe side;
// not needed if your server delivers SVG with correct MIME type
    xhr.overrideMimeType("image/svg+xml");
    xhr.send("");
    document.getElementById("answer--1")
    .appendChild(xhr.responseXML.documentElement);
  })
});
